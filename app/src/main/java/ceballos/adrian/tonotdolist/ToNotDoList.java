package ceballos.adrian.tonotdolist;


public class ToNotDoList {
    public String task;
    public String date;
    public String due;
    public String cate;

    public ToNotDoList (String task,String date, String due, String cate) {
        this.task=task;
        this.date=date;
        this.due=due;
        this.cate=cate;
    }
}
